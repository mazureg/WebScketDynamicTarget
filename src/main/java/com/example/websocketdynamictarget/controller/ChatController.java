package com.example.websocketdynamictarget.controller;

import com.example.websocketdynamictarget.model.ChatMessage;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import static java.lang.String.format;

@Controller
public class ChatController {

    private SimpMessagingTemplate template;


    public ChatController(SimpMessagingTemplate template) {
        this.template = template;
    }

/*    @MessageMapping("/hello")
    @SendTo("/topic/public")
    public ChatMessage sendMessage(@Payload ChatMessage chatMessage) {
        return chatMessage;
    }*/


    @MessageMapping("/hello/{parkingId}")
    public void greet(@DestinationVariable String parkingId, ChatMessage chatMessage) throws Exception {
        template.convertAndSend(format("/topic/%s", parkingId), chatMessage);
    }

}
