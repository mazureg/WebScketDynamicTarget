package com.example.websocketdynamictarget;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebSocketDynamicTargetApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebSocketDynamicTargetApplication.class, args);
	}

}
