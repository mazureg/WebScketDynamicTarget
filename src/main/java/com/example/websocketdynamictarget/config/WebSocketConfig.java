package com.example.websocketdynamictarget.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;

/*
Why do we need STOMP? Well, WebSocket is just a communication protocol.
It doesn’t define things like - How to send a message only to users who are subscribed to a particular topic,
or how to send a message to a particular user. We need STOMP for these functionalities
*/
@Configuration
@EnableWebSocketMessageBroker
@EnableScheduling
public class WebSocketConfig  implements WebSocketMessageBrokerConfigurer {

    @Override
    public void configureMessageBroker(MessageBrokerRegistry config) {

        /*The first line defines that the messages whose destination starts with “/app” should be routed
        to message-handling methods (we’ll define these methods shortly).*/
        config.setApplicationDestinationPrefixes("/app");

       /* And, the second line defines that the messages whose destination starts with “/topic” should be routed
        to the message broker. Message broker broadcasts messages to all the connected clients
        who are subscribed to a particular topic*/
        config.enableSimpleBroker("/topic");
    }

    @Override
    public void registerStompEndpoints(StompEndpointRegistry registry) {
        registry.addEndpoint("/hello").withSockJS();//TODO to przeszkadza ja sie laczymy z klientem javovym
    }
}
